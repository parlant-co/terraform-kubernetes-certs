variable "name" {
  default = "certs"
}
variable "namespace" {
  default = "default"
}
variable "create_namespace" {
  type    = bool
  default = false
}
variable "env" {
  type = map(string)
}


# Kuberentes Certs

Simple job runner that replaces Kubernetes TLS certificates using [acme.sh](https://github.com/acmesh-official/acme.sh).

## Usage

```
module "certs" {
  source = "git@gitlab.com:parlant-co/terraform-kubernetes-certs.git"

  env = {
    # Note: Obtain from https://zerossl.com
    "EAB_HMAC_KEY"  = var.zero_ssl_eab_hmac_key
    "EAB_KID"       = var.zero_ssl_eab_kid
    "HETZNER_Token" = var.hetzner_dns_api_key
  }
}
```

And use the acme.sh annotations on your Ingress:

```
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  annotations:
    acme.kubernetes.io/enable: "true"
    acme.kubernetes.io/dns: dns_hetzner
  name: webservice
spec:
  rules:
  - host: www.webservice.com
    http:
      paths:
      - backend:
          service:
            name: web-service
            port:
              number: 80
        path: /
  tls:
  - hosts:
    - '*.burokrat.eu'
    - burokrat.eu
    secretName: wildcard-webservice-com
```

## Ingress annotations

```
acme.kubernetes.io/enable: "true" | "false"
acme.kubernetes.io/dns: (optional dns provider name)
acme.kubernetes.io/add-args: (optional custom acme.sh arguments)
acme.kubernetes.io/cmd-to-use: (optional custom acme.sh command)
acme.kubernetes.io/staging: (optional "true" | "false", use ACME staging API)
acme.kubernetes.io/pre-cmd: (optional custom sh command to use before renewing certs)
acme.kubernetes.io/post-cmd: (optional custom sh command to use after renewing certs)
acme.kubernetes.io/on-success-cmd: (optional custom sh command to use after successfully renewig certs)
acme.kubernetes.io/on-success-cmd: (optional custom sh command to use on error)
```

### Different DNS providers

In order to use a different DNS provider, please lookup its configuration + name from the [offical acme.sh documentation](https://github.com/acmesh-official/acme.sh/wiki/dnsapi).

For example to use the `dns_hetnzer` provider, you must also configure the `HETZNER_Token` environment variable.
You can specify to use the `dns_hetzner` provider on the Ingress annotation `acme.kubernetes.io/dns: dns_hetzner`.

Anther example is Cloudflare.
There you must configure the `acme.kubernetes.io/dns: dns_cf` annotation and the `CF_Token` and `CF_Account_ID` (and `CF_Zone_ID` if the token is scoped to a single zone) as environment variables.

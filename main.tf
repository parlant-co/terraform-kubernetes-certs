resource "helm_release" "this" {
  name             = var.name
  create_namespace = var.create_namespace
  namespace        = var.namespace
  chart            = "${path.module}/certs-chart"
  recreate_pods    = true

  values = flatten([
    yamlencode({
      env : [
        for key, value in var.env : {
          name  = key
          value = value
        }
      ]
    }),
  ])
}

